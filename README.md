# Monitoramento Provedor de Internet

O intuito do projeto é desenvolver um sistema capaz de fornecer as ferramentas necessárias para o monitoramento e manutenção do bom funcionamento da estrutura de um provedor de acesso à internet, como monitorar a temperatura ou status de um equipamento, consumo de dados, tempo até o esgotamento das baterias de contingência, informar se houve rompimento em algum cabeamento da empresa, além de monitorar a quantidade de clientes conectados e seu consumo, informando se houve queda massiva na conexão de clientes ou algum problema no link das operadores.

Tais medidas serão realizadas por meio da utilização de um sistema para coleta das informações dos equipamentos que fara o armazenamento dos dados em um banco de dados, e posteriormente este será utilizado para fornecer de relatórios em tempo real aos usuários por meio de gráficos e alertas via sms ou e-mail.

INTEGRANTES:

Yan Freire Caser (@yancaser)

Vittor Gomes de Lima (@vittorlima)

David Wilkerson de Oliveira Silva (@davidwilkerson)

Rhuan Fellipe de Souza Cardoso (@rhuan.fellipe)